package application;

/**
 *  This class is for checking the space and directions where the user is
 *  It would check which room where the user is 
 *  and then check the direction whether it is clockwise or counterclockwise
 *  1==bedroom
 *  2==toilet
 *  3==gallery1
 *  4==gallery2
 *  5==kitchen
 *  6==dining room
 * @author  Chenhsi
 * @version 2015.12.3
 */
public class Location {	
  private int location;	
  private String string;
  
  /**
  *  A variable to store the position
  */
  int count = 1;
  private Item item = new Item();
  private Room room = new Room();
  
/**
*  A function to initialize Location
*/
public Location() {}

/**
*  A function to check position(which room) and direction
*/
public String checkdirection(boolean judge, int position){
  location = position;
  boolean right=false;
  right = judge;
   if(location==1 && right==true){ 
     string = room.bedroom(count,"clockwise");
	 count=(count+1)%5;
	}
   else if(location==1 && right==false){
     string = room.bedroom(count,"counterclockwise");
	 if(count>0) count = (count-1)%5;
	 else count=(count+4);	
	  }
		
   if(location==2 && right==true){
	 if(count>=0 && count<=2){
      string = room.toilet(count,"clockwise");
	  count=(count+1)%3;
	   }
	 else{
	  string = room.toilet(1,"clockwise");
	  count=(count+1)%3;	
		}
	}
	else if (location==2 && right==false){
	 if(count>=0 && count<=2){
	  string = room.toilet(count,"counterclockwise");
	  count=(count-1)%3;
		}
	 else{
	  string = room.toilet(1,"counterclockwise");
      count=0;
		}
	}
	
	if(location==3 && right==true){
	 if(count>=0 &&count<=2){
      string = room.gallery(count,"clockwise");
	  count=(count+1)%3;
	   }
	 else{
	  string = room.gallery(1,"clockwise");
	  count=(count+1)%3;
		}
	}
	else if(location==3 && right==false){
	 if(count>0 && count<=2){
	  string = room.gallery(count,"counterclockwise");
	  count = (count-1)%3;
		}
	 else{
	  string = room.gallery(0,"counterclockwise");
	  count=2;
		 }	 
	}
	
	if(location==4 && right==true){
	 if(count>=0 &&count<=3){
      string = room.gallery2(count,"clockwise");
	  count=(count+1)%4;
	   }
	 else{
	  string = room.gallery2(1,"clockwise");
	  count=(count+1)%4;
		}
	}
	else if(location==4 && right==false){
	 if(count>0 && count<=3){
	  string = room.gallery2(count,"counterclockwise");
	  count = (count-1)%3;
		} 
	 else {
      string = room.gallery2(0,"counterclockwise");
	  count=3;
		 }
	}
	
	if(location==5 && right==true){
	 if(count>=0 &&count<=1){
      string = room.kitchen(count,"clockwise");
	  count=(count+1)%2;
	   }
	 else{
	  string = room.kitchen(1,"clockwise");
	  count=(count+1)%2;
		}
	}
	else if(location==5 && right==false){
	 if(count>0 &&count<=1){
	  string = room.kitchen(count,"counterclockwise");
	  count = (count-1)%2;
		}
	 else{
	  string = room.kitchen(count,"counterclockwise");
	  count=(count+1);
			}
		 }			 
	
	if(location==6 && right==true){
	 if(count>=0 &&count<=2){
      string = room.dining(count,"clockwise");
	  count=(count+1)%3;
	   }
	 else{
	  string = room.dining(1,"clockwise");
	  count=(count+1)%3;
		}
	}
	else if(location==6 && right==false){
     string = room.dining(count,"counterclockwise");
	  if(count>0) count = (count-1)%3;
	  else count=(count+2);
	}
	System.out.println("current count = ");
	System.out.println(Integer.toString(count));
	return string;	
  }

/**
*  A function to check whether two adjacent rooms are connected or not
*/
public boolean checkdoor_exist(String tem){
 boolean value;
 value = item.Check_keyexist(tem);
	if(value==true) return true;
	else return false;
}
/**
*  A function to check which room the user is in and return a room number
*/
public int checkroom(String location){
 int setnumber=0;
	if(location=="bedroom1" || location=="bedroom2" || location=="bedroom3" || location=="bedroom4" || location=="bedroom5"){
		setnumber=1;
	}
	if(location=="toilet1" || location=="toilet2" || location=="toilet3"){
	    setnumber=2;	
	}
	if(location=="gallery1"|| location=="gallery2" || location=="gallery3"){
		setnumber=3;
	}
	if(location=="gallery4" || location=="gallery5"|| location=="gallery6"|| location=="gallery7"){
		setnumber=4;
	}
	if(location=="kitchen1" || location=="kitchen2"){
		setnumber=5;
	}
	if(location=="dining1" || location=="dining2" || location=="dining3"){
		setnumber=6;
	}
	return setnumber;
}

}