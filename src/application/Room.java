package application;

/**
 *  This class is for viewing orders of each room, and each room has two viewing directions, and one
 *  is clockwise while the other is counterclockwise. For clockwise direction, the class displays specific 
 *  orders. For counterclockwise direction the class displays diverse order.
 *  
 * @author  Chenhsi
 * @version 2015.12.3
 */
public class Room {
 private String string;
 /**
 *  A function to initialize the Room class
 */
	public Room() {}

/**
*  A function to display bedroom picture in clockwise or counterclockwise directions
*/	
	public String bedroom(int count,String data){
	 String direction = data;
	 int index = count;
	  if(direction=="clockwise"){
        if(index==0)  {string = "bedroom1";}
		if(index==1)  {string = "bedroom2";}
		if(index==2)  {string = "bedroom3";}
		if(index==3)  {string = "bedroom4";}
		if(index==4)  {string = "bedroom5";}
		}
		
	  if(direction=="counterclockwise"){
	    if(index==0)  {string = "bedroom4";}
	    if(index==1)  {string = "bedroom5";}
		if(index==2)  {string = "bedroom1";}
		if(index==3)  {string = "bedroom2";}
		if(index==4)  {string = "bedroom3";}
		}
		return string;
    }
	
/**
*  A function to display toilet picture in clockwise or counterclockwise directions
*/	    
    public String toilet(int count,String data){
	 String direction = data;
	 int index = count;
	  if(direction=="clockwise"){
        if(index==0)  {string = "toilet1";}
		if(index==1)  {string = "toilet2";}
		if(index==2)  {string = "toilet3";}
	    }
	  if(direction=="counterclockwise"){
	    if(index==0)  {string = "toilet2";}
		if(index==1)  {string = "toilet3";}
		if(index==2)  {string = "toilet1";}
	    }
     return string;
    }
    
/**
*  A function to display gallery picture in clockwise or counterclockwise directions
*/	    
    public String gallery(int count,String data){
     String direction = data;
	 int index = count;
	  if(direction=="clockwise"){
        if(index==0)  {string = "gallery3";}
		if(index==1)  {string = "gallery1";}
		if(index==2)  {string = "gallery2";}
	    }
	  if(direction=="counterclockwise"){
	    if(index==0)  {string = "gallery1";}
		if(index==1)  {string = "gallery2";}
		if(index==2)  {string = "gallery3";}
	    }
     return string;
    }
/**
*  A function to display kitchen picture in clockwise or counterclockwise directions
*/	    
    public String kitchen(int count,String data){
     String direction = data;
	 int index = count;
	  if(direction=="clockwise"){
        if(index==0)  {string = "kitchen2";}
		if(index==1)  {string = "kitchen1";}
	    }
	  if(direction=="counterclockwise"){
	    if(index==0)  {string = "kitchen2";}
		if(index==1)  {string = "kitchen1";}
	    }
     return string;
    }
/**
*  A function to display dining room picture in clockwise or counterclockwise directions
*/	    
    public String dining(int count,String data){
     String direction = data;
	 int index = count;
	  if(direction=="clockwise"){
        if(index==0)  {string = "dining1";}
		if(index==1)  {string = "dining2";}
		if(index==2)  {string = "dining3";}
	    }
	 if(direction=="counterclockwise"){
	    if(index==0)  {string = "dining3";}
		if(index==1)  {string = "dining1";}
		if(index==2)  {string = "dining2";}
	 }
     return string;
    }
    
/**
*  A function to display gallery2 picture in clockwise or counterclockwise directions
*/		
    public String gallery2(int count,String data){
     String direction = data;
	 int index = count;
	  if(direction=="clockwise"){
        if(index==0)  {string = "gallery7";}
		if(index==1)  {string = "gallery4";}
		if(index==2)  {string = "gallery5";}
		if(index==3)  {string = "gallery6";}
	 }
	  if(direction=="counterclockwise"){
	    if(index==0)  {string = "gallery4";}
		if(index==1)  {string = "gallery5";}
		if(index==2)  {string = "gallery6";}
		if(index==3)  {string = "gallery7";}
	 }
     return string;
    }
	
}
