package application;

import javafx.application.Application;

import javafx.stage.Stage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
/**
 *  This class is the MainProgram in my assignment
 * @author  Chenhsi
 * @version 2015.12.3
 */
public class MainProgram extends Application {
	
/**
*  A function to set the scene builder
*/	
	public void start(Stage stage) {
		
		try {
            
			FXMLLoader fxmlLoader = new FXMLLoader();
			String viewerFxml = "WorldViewer.fxml";
			AnchorPane page = (AnchorPane) fxmlLoader.load(this.getClass().getResource(viewerFxml).openStream());
			Scene scene = new Scene(page);
			stage.setScene(scene);
			
			MyController controller = (MyController) fxmlLoader.getController();      			
			controller.Initialise();
           
			stage.show();
        
		} catch (IOException ex) {
		   Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		   System.exit(1);
		}
	}
	
/**
*  Just start :)))
*/	
    public static void main(String args[]) {
     	launch(args);
     	System.exit(0);
    }
}
