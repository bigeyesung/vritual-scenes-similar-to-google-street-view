package application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;

/**
*  This class is the main class of my "finding Paul" assignment . 
*  Users can walk through some rooms and watch different angles of each room
*  through "press_right" "press_left" and "press_forward" buttons
*  This main class creates and initialises all the others: it creates Location,
*  Room and Item. It also executes the commands that Item returns.
* 
* @author  Chenhsi
* @version 2015.12.3
*/
public class MyController {

private Location location = new Location();	
private boolean judge=false;
private String tem;
private Item item = new Item();
private boolean existence;
private int item_number=0;
private Image monster_return;
	
	@FXML
	private ImageView pic_rabbit;

	@FXML
	private ImageView pic_view;
	
	@FXML
	private ImageView pic_map;
	
	@FXML
	private ImageView pic_money;
	
	@FXML
	private ImageView pic_catwoman;
	
	@FXML
	private ImageView pic_paul;
	
	@FXML
	private ImageView pic_item1;
	
	@FXML
	private ImageView pic_item2;
	
	@FXML
	private Button pic_catch;
	
	@FXML
	private Button pic_put;
	
	@FXML
	private Button button_forward;
	
	@FXML
	private TextArea message;
	
/**
*A function to create and initialize MyController
*/
	public MyController(){};
	
 /**
*A function to show the message
*/	  
    public void say(ActionEvent event){
     message.setText("Welcomg to the game finding Paul!");
    }

 /**
*A function to set the default view
*/   
	public void Initialise() {
     tem="bedroom1";
	  if(item.monster_room_exist(tem)){
		String monster = item.monster_room(tem);
		Image show_monster = item.key_match(monster);
		Image room= item.key_match(tem);
		Image map = item.room_map_match(tem);
		pic_view.setImage(room);
		pic_rabbit.setImage(show_monster);
		pic_map.setImage(map);
        monster_return=show_monster;
        pic_put.setDisable(true);
        button_forward.setDisable(true);
        message.setText("Welcomg to the game finding Paul!");
	    existence=true;
		}

	}

/**
*A function to the right button action
*/	
	public void press_right(ActionEvent event) {

		int position = location.checkroom(tem);
		judge=true;
		String string = location.checkdirection(judge,position);
		tem = string;
		Image map = item.room_map_match(tem);
		if(item.Check_keyexist(tem)==true) button_forward.setDisable(false);
		else button_forward.setDisable(true);
		 if(item.monster_room_exist(tem)){
		  if(existence==true){
		   String monster = item.monster_room(tem);
		   Image show_monster = item.key_match(monster);
		   Image room= item.key_match(tem);
			if(tem=="bedroom1")pic_rabbit.setVisible(true);
			if(tem=="toilet1") pic_money.setImage(show_monster);
			if(tem=="gallery1") pic_catwoman.setImage(show_monster);
			if(tem=="dining1") pic_paul.setImage(show_monster);
			pic_map.setImage(map);
			pic_view.setImage(room);
			monster_return=show_monster;
			} 
		   else{
			String monster = item.monster_room(tem);
			Image show_monster = item.key_match(monster);
			Image room= item.key_match(string);
			pic_view.setImage(room);
			pic_rabbit.setVisible(false);
			if(tem=="toilet1") pic_money.setImage(show_monster);
			if(tem=="gallery1") pic_catwoman.setImage(show_monster);
			if(tem=="dining1") pic_paul.setImage(show_monster);
			pic_map.setImage(map);
			monster_return=show_monster;
			}
		}
		else{
			Image room= item.key_match(string);
		    pic_rabbit.setImage(null);
		    pic_money.setImage(null);
		    pic_catwoman.setImage(null);
		    pic_paul.setImage(null);
            pic_view.setImage(room);
            pic_map.setImage(map);
		}

	}
	
/**
*A function to the right left action
*/	
	public void press_left(ActionEvent event){
	
		int position = location.checkroom(tem);
		String string = location.checkdirection(false,position);
		tem = string;
		Image map = item.room_map_match(tem);
		if(item.Check_keyexist(tem)==true) button_forward.setDisable(false);
		else button_forward.setDisable(true);
		 if(item.monster_room_exist(tem)){
		  if(existence==true){
			String monster = item.monster_room(tem);
			Image show_monster = item.key_match(monster);
			Image room= item.key_match(string);
			pic_view.setImage(room);
			if (tem=="bedroom1") pic_rabbit.setVisible(true);
			if(tem=="toilet1") pic_money.setImage(show_monster);
			if(tem=="gallery1") pic_catwoman.setImage(show_monster);
			if(tem=="dining1") pic_paul.setImage(show_monster);
			pic_map.setImage(map);
			monster_return=show_monster;
			}
		  else{
				String monster = item.monster_room(tem);
				Image show_monster = item.key_match(monster);
				Image room= item.key_match(string);
				pic_view.setImage(room);
				pic_rabbit.setVisible(false);
				if(tem=="toilet1") pic_money.setImage(show_monster);
				if(tem=="gallery1") pic_catwoman.setImage(show_monster);
				if(tem=="dining1") pic_paul.setImage(show_monster);
				pic_map.setImage(map);
				monster_return=show_monster;
			}
		}
		else{
		    Image room = item.key_match(string);
		    pic_rabbit.setImage(null);
	        pic_money.setImage(null);
	        pic_catwoman.setImage(null);
	        pic_paul.setImage(null);
            pic_view.setImage(room);
            pic_map.setImage(map);
		}
	}

/**
*A function to the forward button action
*/		
	public void press_forward(ActionEvent event){
	
	 judge=location.checkdoor_exist(tem);
	 if(item.Check_keyexist(tem)==true) button_forward.setDisable(false);
	 else button_forward.setDisable(true);
	  if(judge==true){
		String match = item.room_connection(tem);

		 if(item.monster_room_exist(match)){
			if(existence==true){
				Image picture = item.key_match(match);
				tem = match; 
				String monster = item.monster_room(tem);
				Image show_monster = item.key_match(monster);
				Image map = item.room_map_match(tem);
				if(tem=="bedroom1")pic_rabbit.setVisible(true);
				if(tem=="toilet1") pic_money.setImage(show_monster);
				if(tem=="gallery1") pic_catwoman.setImage(show_monster);
				if(tem=="dining1") pic_paul.setImage(show_monster);
				pic_view.setImage(picture);
				pic_map.setImage(map);
				monster_return=show_monster;
			}
			else{
				Image picture = item.key_match(match);
				tem = match; 
				String monster = item.monster_room(tem);
				Image show_monster = item.key_match(monster);
				Image map = item.room_map_match(tem);
				if(tem=="toilet1") pic_money.setImage(show_monster);
				if(tem=="gallery1") pic_catwoman.setImage(show_monster);
				if(tem=="dining1") pic_paul.setImage(show_monster);
				pic_rabbit.setVisible(false);
				pic_view.setImage(picture);
				pic_map.setImage(map);
				monster_return=show_monster;
			}
			
		 }
		 else{
				tem=match;
			    Image room= item.key_match(tem);
				Image map = item.room_map_match(tem);
			    pic_rabbit.setImage(null);
			    pic_money.setImage(null);
			    pic_catwoman.setImage(null);
			    pic_paul.setImage(null);
	            pic_view.setImage(room);
	            pic_map.setImage(map);
		 }
		 
	   }
	   else
		button_forward.setDisable(true);
		
		
	}
	
/**
*A function to the catching button action
*/	
    public void catching(ActionEvent event){

     pic_put.setDisable(false);
      if(item_number>=0 && item_number<2){
     	if(tem=="bedroom1") pic_rabbit.setVisible(false);
     	if(tem=="toilet1") pic_money.setVisible(false);
     	if(tem=="gallery1") pic_catwoman.setVisible(false);
     	if(tem=="dining1") {
     		pic_paul.setVisible(false);
     		message.setText("Now you captured Paul,but you have me give up 'money' "
     				+ "or 'woman', is it worthwhile to do so?");
     	}
     	existence = false;
     	item_number++;
     	if(item_number==1) pic_item1.setImage(monster_return);
     	if(item_number==2) {
     		pic_item2.setImage(monster_return);
     		pic_catch.setDisable(true);
     	}	
     }
      else{
        pic_catch.setDisable(true); 
     }
    }
    
/**
*A function to the put back button action
*/  
	public void putback(ActionEvent event){

		if(item_number==2){
		 pic_item2.setImage(null);
		 pic_catch.setDisable(false);
			if(tem=="bedroom1") {pic_rabbit.setVisible(true);}	
			if(tem=="toilet1")  {pic_money.setVisible(true);}	
	    	if(tem=="gallery1") {pic_catwoman.setVisible(true);}
	    	if(tem=="dining1")  {pic_paul.setVisible(true);}	
		 existence = true;
		 item_number--;
		}
		else if(item_number==1){
		 pic_item2.setImage(null);
		 pic_item1.setImage(null);
		 pic_catch.setDisable(false);
			if(tem=="bedroom1") {pic_rabbit.setVisible(true);}
			if(tem=="toilet1")  {pic_money.setVisible(true);}
	    	if(tem=="gallery1") {pic_catwoman.setVisible(true);}
	    	if(tem=="dining1")  {pic_paul.setVisible(true);}
		existence = true;
		item_number--;
		}
		else{
		 pic_put.setDisable(true);
		 pic_catch.setDisable(false);
		}
	}
  
}