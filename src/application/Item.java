package application;
import java.util.HashMap;
import javafx.scene.image.Image;

/**
 *  This class is mainly for storing images and assign each image a name
 *  Also it creates hashmaps using a key to match a corresponding value such as 
 *  "hashmap" which is for finding a corresponding image, and "hashmap_connection" which is  
 *  for the space connection of each room, and "hashmap_monster" which is for some rooms with monsters.
 *  And then return the data to MyController
 * @author  Chenhsi
 * @version 2015.12.3
 */
public class Item {
private HashMap<String,Image> hashmap = new HashMap<String,Image>();
private HashMap<String,Image> hashmap_map_room = new HashMap<String,Image>();
private HashMap<String,String> hashmap_connection = new HashMap<String,String>();
private HashMap<String,String> hashmap_moster_room = new HashMap<String,String>();
/**
*  A function to initialize Item class and put images
*/	
	public Item() {
		
		Image rabbit = new Image(getClass().getResourceAsStream("rabbit.png"));
		Image paul = new Image(getClass().getResourceAsStream("paul.jpg"));
		Image catwoman = new Image(getClass().getResourceAsStream("catwoman.png"));
		Image money = new Image(getClass().getResourceAsStream("money.PNG"));
		Image up = new Image(getClass().getResourceAsStream("up.png"));
		Image left = new Image(getClass().getResourceAsStream("left.png"));
		Image right = new Image(getClass().getResourceAsStream("right.png"));
		Image catching = new Image(getClass().getResourceAsStream("catch.png"));
		Image putdown = new Image(getClass().getResourceAsStream("putdown.png"));
		
		Image b1 = new Image(getClass().getResourceAsStream("b1.png"));
		Image b2 = new Image(getClass().getResourceAsStream("b2.png"));
		Image b3 = new Image(getClass().getResourceAsStream("b3.png"));
		Image b4 = new Image(getClass().getResourceAsStream("b4.png"));
		Image b5 = new Image(getClass().getResourceAsStream("b5.png"));
		Image t1 = new Image(getClass().getResourceAsStream("t1.png"));
		Image t2 = new Image(getClass().getResourceAsStream("t2.png"));
		Image t3 = new Image(getClass().getResourceAsStream("t3.png"));
		Image g1 = new Image(getClass().getResourceAsStream("g1.png"));
		Image g2 = new Image(getClass().getResourceAsStream("g2.png"));
		Image g3 = new Image(getClass().getResourceAsStream("g3.png"));
		Image g4 = new Image(getClass().getResourceAsStream("g4.png"));
		Image g5 = new Image(getClass().getResourceAsStream("g5.png"));
		Image g6 = new Image(getClass().getResourceAsStream("g6.png"));
		Image g7 = new Image(getClass().getResourceAsStream("g7.png"));
		Image k1 = new Image(getClass().getResourceAsStream("k1.png"));
		Image k2 = new Image(getClass().getResourceAsStream("k2.png"));
		Image d1 = new Image(getClass().getResourceAsStream("d1.png"));
		Image d2 = new Image(getClass().getResourceAsStream("d2.png"));
		Image d3 = new Image(getClass().getResourceAsStream("d3.png"));
		Image bedroom1 = new Image(getClass().getResourceAsStream("bedroom1.JPG"));
		Image bedroom2 = new Image(getClass().getResourceAsStream("bedroom2.JPG"));
		Image bedroom3 = new Image(getClass().getResourceAsStream("bedroom3.JPG"));
		Image bedroom4 = new Image(getClass().getResourceAsStream("bedroom4.JPG"));
		Image bedroom5 = new Image(getClass().getResourceAsStream("bedroom5.JPG"));
		Image toilet1 = new Image(getClass().getResourceAsStream("toilet1.JPG"));
		Image toilet2 = new Image(getClass().getResourceAsStream("toilet2.JPG"));
		Image toilet3 = new Image(getClass().getResourceAsStream("toilet3.JPG"));		
		Image gallery1 = new Image(getClass().getResourceAsStream("gallery1.JPG"));
		Image gallery2 = new Image(getClass().getResourceAsStream("gallery2.JPG"));
		Image gallery3 = new Image(getClass().getResourceAsStream("gallery3.JPG"));
		Image gallery4 = new Image(getClass().getResourceAsStream("gallery4.JPG"));
		Image gallery5 = new Image(getClass().getResourceAsStream("gallery5.JPG"));
		Image gallery6 = new Image(getClass().getResourceAsStream("gallery6.JPG"));		
		Image kitchen1 = new Image(getClass().getResourceAsStream("kitchen1.JPG"));
		Image kitchen2 = new Image(getClass().getResourceAsStream("kitchen2.JPG"));		
		Image dining1 = new Image(getClass().getResourceAsStream("dining1.JPG"));
		Image dining2 = new Image(getClass().getResourceAsStream("dining2.JPG"));
		Image dining3 = new Image(getClass().getResourceAsStream("dining3.JPG"));
		
		hashmap.put("bedroom1", bedroom1);
		hashmap.put("bedroom2", bedroom2);
		hashmap.put("bedroom3", bedroom3);
		hashmap.put("bedroom4", bedroom4);
		hashmap.put("bedroom5", bedroom5);
		hashmap.put("toilet1", toilet1);
		hashmap.put("toilet2", toilet2);
		hashmap.put("toilet3", toilet3);
		hashmap.put("gallery1", gallery1);
		hashmap.put("gallery2", gallery2);
		hashmap.put("gallery3", gallery3);
		hashmap.put("gallery4", gallery4);
		hashmap.put("gallery5", gallery5);
		hashmap.put("gallery6", gallery6);
		hashmap.put("gallery7", gallery1);
		hashmap.put("kitchen1", kitchen1);
		hashmap.put("kitchen2", kitchen2);
		hashmap.put("dining1", dining1);
		hashmap.put("dining2", dining2);
		hashmap.put("dining3", dining3);		
		hashmap.put("rabbit", rabbit);
		hashmap.put("money", money);
		hashmap.put("catwoman", catwoman);
		hashmap.put("paul", paul);		
		hashmap.put("up", up);
		
		hashmap_map_room.put("bedroom1", b1);
		hashmap_map_room.put("bedroom2", b2);
		hashmap_map_room.put("bedroom3", b3);
		hashmap_map_room.put("bedroom4", b4);
		hashmap_map_room.put("bedroom5", b5);
		hashmap_map_room.put("toilet1", t1);
		hashmap_map_room.put("toilet2", t2);
		hashmap_map_room.put("toilet3", t3);
		hashmap_map_room.put("gallery1", g1);
		hashmap_map_room.put("gallery2", g2);
		hashmap_map_room.put("gallery3", g3);
		hashmap_map_room.put("gallery4", g4);
		hashmap_map_room.put("gallery5", g5);
		hashmap_map_room.put("gallery6", g6);
		hashmap_map_room.put("gallery7", g7);
		hashmap_map_room.put("kitchen1", k1);
		hashmap_map_room.put("kitchen2", k2);
		hashmap_map_room.put("dining1", d1);
		hashmap_map_room.put("dining2", d2);
		hashmap_map_room.put("dining3", d3);
		
		hashmap_connection.put("bedroom3", "toilet1");
		hashmap_connection.put("bedroom2", "gallery1");
		hashmap_connection.put("toilet3", "bedroom2");
		hashmap_connection.put("gallery1", "gallery5");
		hashmap_connection.put("gallery6", "kitchen1");
		hashmap_connection.put("kitchen2", "dining1");
		hashmap_connection.put("dining3", "gallery5");
		hashmap_connection.put("gallery7", "gallery2");
		hashmap_connection.put("gallery2", "bedroom1");
		hashmap_moster_room.put("bedroom1", "rabbit");
		hashmap_moster_room.put("toilet1", "money");
		hashmap_moster_room.put("gallery1", "catwoman");
		hashmap_moster_room.put("dining1", "paul");
		
	}
	
/**
*  A function to assign a room to its corresponding picture
*/	
public Image key_match(String roomname){
	return hashmap.get(roomname);	
}

/**
*  A function to assign previous room to its adjacent room
*/	
public String room_connection(String previous_room){
	return hashmap_connection.get(previous_room);
}

/**
*  A function to check if a key exists 
*/	
public boolean Check_keyexist(String key){
 boolean value=false;
 value = hashmap_connection.containsKey(key);
	if(value==true) return true;
	else return false;
}

/**
*  A function to return monster picture if it is a monster-room
*/	
public String monster_room(String room){
	return hashmap_moster_room.get(room);
}

/**
*  A function to check if it is a monster-room
*/	
public boolean monster_room_exist(String roomname){
 boolean value = false;
 value = hashmap_moster_room.containsKey(roomname);
	if(value==true) return true;
	else return false;
}

/**
*  A function to return map to its matched room
*/	
public Image room_map_match(String roomname){
	return hashmap_map_room.get(roomname);
}

}
